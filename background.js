function logURL(requestDetails) {
    console.log("Loading: " + requestDetails.url);
}

function block_googleAnalytics(details) {
    // console.log("Loading: " + details.url);
    var toBlock = false;
    if (details.url.includes("google-analytics") ||
        details.url.includes("googletagmanager") 
    ) {
        toBlock = true;
        console.log(`blocking ${details.url}`);
    }

    return { cancel: toBlock };
}

function intercept(requestDetails){
    return block_googleAnalytics(requestDetails);
}

chrome.webRequest.onBeforeRequest.addListener(
    intercept,
    {
        urls: ["<all_urls>",]
    },
    ["blocking"]
);

// var pattern = "https://mdn.mozillademos.org/*";

// function redirect(requestDetails) {
//     console.log("Redirecting: " + requestDetails.url);
//     return {
//         redirectUrl: "https://38.media.tumblr.com/tumblr_ldbj01lZiP1qe0eclo1_500.gif"
//     };
// }

// browser.webRequest.onBeforeRequest.addListener(
//     redirect,
//     { urls: [pattern], types: ["image"] },
//     ["blocking"]
// );

console.log(`intercepting http request`);